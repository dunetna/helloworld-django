from django.shortcuts import render

def hello(request):
    return render(request, 'main/hello.html')

def bye(request):
    return render(request, 'main/bye.html')

def index(request):
    return render(request, 'main/index.html')
